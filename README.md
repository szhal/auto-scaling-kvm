# Getting Started
This is a project to implement auto scaling in QEMU/KVM virtual machines. The auto scaling method used is `Threshold-based`, with CPU as the evaluated metric. The monitoring tools used are Prometheus, Alert Manager, and Grafana. The VM is launched using the cloud init method.

# Prerequisites
- Ubuntu 20.04 LTS
- QEMU/KVM and Dependencies
  ```
  sudo apt install -y qemu-kvm \
                      libvirt-daemon-system \
                      libvirt-clients \
                      bridge-utils \
                      virtinst \
                      genisoimage \
                      libguestfs-tools
  ```
- Prometheus (up and running)
- Alert Manager (up and running)
- Grafana (up and running)
- Adnanh Webhook (up and running)

# Usage
All implementation is written through [issues](https://gitlab.com/szhal/auto-scaling-kvm/-/issues) with details as follows: <br>

1. Prepare QEMU/KVM Environment

   This section described steps to `sanity check` the environment that wants to be implemented.

 
